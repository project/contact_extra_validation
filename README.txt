***********
* README: *
***********

DESCRIPTION:
------------
This module gives you extra validation for contact form.

-- REQUIREMENTS --

contact module

INSTALLATION:

1. Place the entire contact_extra_validation directory into your Drupal modules/
   directory or the sites modules directory (eg sites/all/modules/)


2. Enable this module by navigating to:

     Administration > Modules

3. Configure of Contact extra validation by visiting:

     Administration » Structure » Contact form
